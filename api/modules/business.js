import {
	req
} from '../../utils/req.js';
export default {
	// 获取门店信息
	getStore(param) {
		return req.get("massage/app/IndexStore/getStore", param)
	},
	// 门店申请
	apply(param) {
		return req.post("massage/app/IndexStore/apply", param)
	},
	// 门店修改
	edit(param) {
		return req.post("massage/app/IndexStore/edit", param)
	},
	// 门店列表
	getList(param) {
		return req.get("massage/app/IndexStore/getList", param)
	},
	// 门店详情
	getInfo(param) {
		return req.get("massage/app/IndexStore/getInfo", param)
	},
}
